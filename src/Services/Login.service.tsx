import { auth } from 'firebase';

const googleProvider = new auth.GoogleAuthProvider();
const facebookProvider = new auth.FacebookAuthProvider();

const RedirectResult = () => auth().getRedirectResult();

const GoogleSignin = () => auth().signInWithPopup(googleProvider);
const FacebookSignin = () => auth().signInWithPopup(facebookProvider);
const EmailSignin = (email: string, password: string) => auth().signInWithEmailAndPassword(email, password);
const EmailSignup = (email: string, password: string) => auth().createUserWithEmailAndPassword(email, password);
const Logout = () => auth().signOut();

export {
  EmailSignin,
  EmailSignup,
  FacebookSignin,
  GoogleSignin,
  Logout,
  RedirectResult,
};
