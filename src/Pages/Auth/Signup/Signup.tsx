import { CheckCircleTwoTone, SmileTwoTone, LockTwoTone, UnlockTwoTone, GoogleOutlined } from '@ant-design/icons';
import React, { ChangeEvent, useState } from 'react';
import { RouteComponentProps } from 'react-router';
import FitButton from '../../../Components/Button/FitButton';
import FitInput from '../../../Components/Input/Input';
import { IFitButton } from '../../../Interfaces/FitButton.interface';
import { EmailSignup, GoogleSignin } from '../../../Services/Login.service';
import './Signup.scss';

const FitSignup = (props: RouteComponentProps) => {
  const fitSignupButtonData: IFitButton = {
    title: 'Signup!',
    className: 'SignupButton',
    type: 'default',
    shape: 'round',
    htmlType: 'submit',
  };
  const fitGoogleButtonData: IFitButton = {
    title: '',
    className: 'GoogleSigninButton',
    type: 'default',
    shape: 'circle',
    htmlType: 'submit',
    size: 'large',
    danger: true,
    icon: <GoogleOutlined />,
  };
  const [emailInput, setEmailInput] = useState(() => {
    return {
      rotate: 0,
      twoToneColor: '',
      value: '',
      valid: false,
    };
  });
  const [passwordInput, setPasswordInput] = useState(() => {
    return {
      twoToneColor: '',
      value: '',
      valid: false,
    };
  });
  const [fitButtonState, setFitSignupButtonState] = useState(() => fitSignupButtonData);
  const [fitGoogleButtonState] = useState(() => fitGoogleButtonData);
  const changeEventHandler = (event: ChangeEvent<HTMLInputElement>) => {
    let emailString = event.target.value;
    setEmailInput(() => {
      if (validateEmail(emailString)) {
        return {
          rotate: 0,
          twoToneColor: 'blue',
          value: emailString,
          valid: true,
        };
      } else {
        return {
          rotate: 180,
          twoToneColor: 'red',
          value: emailString,
          valid: false,
        };
      }
    });
  };
  const focusEventHandler = (event: React.FocusEvent) => {
    const e = event.target as HTMLInputElement;
    if (e.type === 'email') {
      const emailStyle = emailInput.valid
        ? { ...emailInput }
        : { ...emailInput, rotate: 180, twoToneColor: 'red' };
      setEmailInput(emailStyle);
    } else {
      passwordInput.valid
        ? setPasswordInput({ ...passwordInput, twoToneColor: 'blue' })
        : setPasswordInput({ ...passwordInput, twoToneColor: 'red' });
    }
  };
  const passwordChangeEventHandler = (event: ChangeEvent<HTMLInputElement>) => {
    let passwordString = event.target.value;
    setPasswordInput((prevState) => {
      if (passwordString.length > 6) {
        return {
          twoToneColor: 'blue',
          value: passwordString,
          valid: true,
        };
      } else {
        return {
          twoToneColor: 'red',
          valid: false,
          value: passwordString,
        };
      }
    });
  };
  const clickEventHandler = () => {
    setFitSignupButtonState({ ...fitSignupButtonData, loading: true });
    EmailSignup(emailInput.value, passwordInput.value).then((res) => {
      setFitSignupButtonState({
        ...fitSignupButtonData,
        shape: 'circle',
        loading: false,
        title: '',
        icon: <CheckCircleTwoTone twoToneColor='#52c41a' />,
      });
      props.history.push('/');
    });
  };
  const googleClickEventHandler = () => {
    GoogleSignin().then((res) => {
      console.log(res);
      
      props.history.push('/');
    });
  };
  const validateEmail = (email: string) => {
    var regex = /\S+@\S+\.\S+/;
    return regex.test(String(email).toLowerCase());
  };

  return (
    <div className="FitSignup">
      <FitInput
        placeholder='Email'
        type='email'
        onChange={changeEventHandler}
        onFocus={focusEventHandler}
        bordered={false}
        size='large'
        value={emailInput.value}
        prefix={
          <SmileTwoTone
            rotate={emailInput.rotate}
            twoToneColor={emailInput.twoToneColor}
          />
        }
      ></FitInput>
      <FitInput
        placeholder='Password'
        type='password'
        value={passwordInput.value}
        onChange={passwordChangeEventHandler}
        onFocus={focusEventHandler}
        bordered={false}
        size='large'
        prefix={
          passwordInput.valid ? (
            <LockTwoTone twoToneColor={passwordInput.twoToneColor} />
          ) : (
            <UnlockTwoTone twoToneColor={passwordInput.twoToneColor} />
          )
        }
      ></FitInput>
      <div className="FitSignup__buttons">
        <FitButton {...fitButtonState} onClick={clickEventHandler}></FitButton>
        <hr/>
        <span> Or </span>
        <FitButton
          {...fitGoogleButtonState}
          onClick={googleClickEventHandler}
        ></FitButton>
      </div>
    </div>
  );
};

export default FitSignup;