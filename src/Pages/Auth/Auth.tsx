import React from 'react';
import { Switch, Route } from 'react-router-dom';
import AUTHROUTES from './AuthRoutes';
import { IFitRoute } from '../../Interfaces/FitRoute.interface';

const Auth = () => {
  return (
    <div>
      <Switch>
        {AUTHROUTES.map((route: IFitRoute) => {
          return <Route {...route} />;
        })}
      </Switch>
      {/* <FitSignin></FitSignin> */}
    </div>
  );
}

export default Auth;