import { IFitRoute } from '../../Interfaces/FitRoute.interface';
import FitSignin from './Signin/Signin';
import FitSignup from './Signup/Signup';

const AUTHROUTES: IFitRoute[] = [
  {
    path: '/auth',
    exact: true,
    key: 'FIT_AUTH',
    component: FitSignin
  },
  {
    path: '/auth/signup',
    exact: true,
    key: 'FIT_SIGNIN',
    component: FitSignup,
  },
  {
    path: '/auth/signin',
    exact: true,
    key: 'FIT_SIGNUP',
    component: FitSignin,
  },
];

export default AUTHROUTES;

