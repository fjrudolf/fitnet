import React from 'react';
import FitButton from '../../Components/Button/FitButton';
import './Home.scss';

const FitHome = () => {
  const clickHandler = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    console.log(event);
  };

  // TODO: Check if user is logged in, if not redirect to Landing
  return (
    <div className='FitHome'>
      <img className="FitHome__image" src="./assets/img/animation_500_keep0len.gif" alt=""/>
      <span> This is the home component </span>
      <FitButton
        title={'I am a button'}
        type={'primary'}
        onClick={clickHandler}
        htmlType="button"
      />
    </div>
  );
}

export default FitHome;
