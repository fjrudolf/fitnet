import './Landing.scss';
import React from 'react';
import FitButton from '../../Components/Button/FitButton';

const FitLanding = () => {
  const clickHandler = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
  };
  return (
    <div className='FitLanding'>
      Landing
      <img src='./assets/img/animation_500_keeol7wm.gif' alt='' />
      <FitButton
        title={'I am a button'}
        type={'primary'}
        onClick={clickHandler}
        htmlType='button'
      />
    </div>
  );
};

export default FitLanding;
