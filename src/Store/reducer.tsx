import { Action, Reducer } from 'redux';
import * as actionTypes from './Actions';
import { IFitAthlete } from '../Interfaces/FitAthlete.interface';

// TODO: Configure store
const initialState = {
  fitAthlete: {
  },
  fitGym: {
    
  }
  
}

const reducer: Reducer<unknown, Action<any>> = (state = initialState, action: any) => {
  switch (action.type) {
    case actionTypes.SET_USER:
      
      break;
    case actionTypes.REMOVE_USER:
      
      break;
  
    default:
      break;
  }
};

export default reducer;