import React from 'react';
import './Input.scss';
import { Input } from 'antd';
import { IFitInput } from '../../Interfaces/FitInputInterface';

const FitInput = (props: IFitInput) => {
  return (
    <Input
      className="FitInput"
      {...props}
    ></Input>
  );
};

export default FitInput;
