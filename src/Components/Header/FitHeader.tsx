import './FitHeader.scss';
import React from 'react';
import { IFitHeader, IFitHeaderItem } from '../../Interfaces/FitHeader.interface';
import { Menu } from 'antd';
import ROUTES from '../../AppRoutes';
import { Link } from 'react-router-dom';

const FitHeader = (props: IFitHeader) => {
  console.log(props)
  const itemClicked = (event: any) => {
    props.onClick(event);
  };
  const navigate = (item: IFitHeaderItem) => {
    let path = ROUTES.find((route) => route.key === item.key);
    return <Link to={path ? path.path : '/'}> { item.title } </Link> 
  }
  return (
    <Menu
      { ...props }
      onClick={ itemClicked }>
      {
        props.menuitems?.map((item) => {
          return <Menu.Item { ...item }>{ navigate(item) }</Menu.Item>;
        })
      }
    </Menu>
  );
};

export default FitHeader;
