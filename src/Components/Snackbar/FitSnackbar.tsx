import React from 'react';
import './FitSnackbar.scss';
import { message } from 'antd';
import { IFitSnackbar } from '../../Interfaces/FitSnackbar.interface';

export const FitSnackbar = (props: IFitSnackbar) => {
  return ( message[props.type](`${props.message}`))
}
