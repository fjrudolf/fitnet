import React from 'react';
import './FitButton.scss';
import { Button } from 'antd';
import { IFitButton } from '../../Interfaces/FitButton.interface';

const FitButton = (props: IFitButton) => {
  return (
    <Button
      {...props}
      className={props.className}
      onClick={props.onClick}
    >
      {props.title}
    </Button>
  );
};

export default FitButton;
