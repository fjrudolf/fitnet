import React from 'react';
import firebase from 'firebase';
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './Store/Reducer';
import FitHeader from './Components/Header/FitHeader';
import ROUTES from './AppRoutes';
import { IFitHeader } from './Interfaces/FitHeader.interface';
import './App.scss';
import 'antd/dist/antd.css';
import { IFitRoute } from './Interfaces/FitRoute.interface';

// Firebase config
const firebaseConfig = {
  apiKey: 'AIzaSyBq160Co-JUcl6EVr_QERTe2BIdHoZgjFc',
  authDomain: 'fitnet-40365.firebaseapp.com',
  databaseURL: 'https://fitnet-40365.firebaseio.com',
  projectId: 'fitnet-40365',
  storageBucket: 'fitnet-40365.appspot.com',
  messagingSenderId: '869734413973',
  appId: '1:869734413973:web:74ff4d99bcddc5d7bb8d9c',
  measurementId: 'G-Y3P1PPX0L7',
};
firebase.initializeApp(firebaseConfig);

// Redux store config
const store = createStore(reducer)

const menuItemHandler = (event: React.MouseEvent | any) => {
  let navigate = ROUTES.filter(route => route.key === event.key);
  console.log(navigate, navigate[0].path);
  return (<Link to={navigate[0].path}></Link>);
};

const headerItems: IFitHeader = {
  menuitems: [
    {
      disabled: false,
      key: 'FIT_SIGNUP',
      title: 'Signup!',
      icon: '',
      danger: false,
    },
    {
      disabled: false,
      key: 'FIT_SIGNIN',
      title: 'Signin!',
      icon: '',
      danger: false,
    },
  ],
  imgsrc: '',
  onClick: menuItemHandler,
  mode: 'horizontal',
  className: 'App__header',
  theme: 'dark',
};

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className='App'>
          <FitHeader {...headerItems} ></FitHeader>
          <div className="App__main">
            <Switch>
              {
                ROUTES.map((route: IFitRoute) => {
                  return (
                      <Route {...route} />
                  );
                })
              }
              <Route component={() => <h1>Not Found!</h1>} />
            </Switch>
          </div>
        </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
