import React from 'react';
import { Route, Switch } from 'react-router';
import { IFitRoute, IFitRoutes } from './Interfaces/FitRoute.interface';
import FitLanding from './Pages/Landing/Landing';
import FitHome from './Pages/Home/Home';
import Auth from './Pages/Auth/Auth';

const ROUTES: IFitRoute[] = [
  {
    path: '/',
    key: 'FIT_ROOT',
    exact: true,
    component: FitHome,
  },
  {
    path: '/landing',
    key: 'FIT_LANDING',
    component: FitLanding,
  },
  {
    path: '/auth',
    key: 'FIT_AUTH',
    component: Auth,
  },
  {
    path: '/auth/signin',
    exact: true,
    key: 'FIT_SIGNIN',
    component: Auth,
  },
  {
    path: '/auth/signup',
    exact: true,
    key: 'FIT_SIGNUP',
    component: Auth,
  },
];

function RouteWithSubRoutes(route: IFitRoute) {
  return (
    <Route
      path={ route.path }
      exact={ route.exact }
      render={ (props) => <route.component { ...props } routes={ route.children } /> }
    />
  );
}

export function RenderRoutes(props: IFitRoutes) {
  const routes: IFitRoute[] = props.routes;
  return (
    <Switch>
      {
        routes.map((route: IFitRoute) => {
          return (<RouteWithSubRoutes { ...route } />);
        })
      }
      <Route component={() => <h1>Not Found!</h1>} />
    </Switch>
  );
}

export default ROUTES;
