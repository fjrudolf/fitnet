import { IFitClass } from './FitClass.interface';

export interface IFitGymUser {
  name: string;
  address: string;
  contact: string;
  id: string;
  classes: IFitClass[];
}
