import { ChangeEvent, ReactNode } from 'react';

export interface IFitInput {
  placeholder: string;
  type: string;
  value: string;
  bordered?: boolean;
  prefix?: string | ReactNode;
  size?: 'large' | 'small';
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  onFocus?: (event: React.FocusEvent<HTMLInputElement>) => void;
}
