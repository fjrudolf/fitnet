import React from 'react';

export interface IFitRoute {
  path: string;
  key: string;
  component: React.Component | React.FunctionComponent | any; // Need to remove any type from here.
  exact?: boolean;
  children?: IFitRoute[];
}

export interface IFitRoutes {
  routes: IFitRoute[];
}