export interface IFitClass {
  name: string;
  duration: Number;
  fitcoinValue: string;
  disciplin: string;
  elements: string;
  trainer?: string;
}

export interface IFitClassLive extends IFitClass {
  date: Date;
  maxParticipants: number;
}
