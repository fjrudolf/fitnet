import { IFitClass } from './FitClass.interface';

export interface IFitAthlete {
  name: string;
  id: string;
  email: string;
  preferences: IFitClass[];
  profileImage: string;
  fitcoinsAvailable: number;
}
