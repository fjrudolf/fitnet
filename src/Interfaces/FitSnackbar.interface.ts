export interface IFitSnackbar {
  message: string;
  type: 'success' | 'error' | 'warning'
}