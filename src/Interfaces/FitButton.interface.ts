import { ReactNode } from 'react';

export interface IFitButton {
  title: string;
  htmlType: 'button' | 'submit' | 'reset' | undefined;
  className?: string;
  shape?: 'circle' | 'round';
  size?: 'large' | 'small' | 'middle';
  icon?: ReactNode;
  loading?: boolean | { delay: number };
  danger?: boolean;
  onClick?: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void;
  type: 'text' | 'link' | 'ghost' | 'default' | 'primary' | 'dashed';
}
