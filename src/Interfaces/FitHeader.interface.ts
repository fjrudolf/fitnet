import { ReactNode } from 'react';

export interface IFitHeader {
  menuitems: IFitHeaderItem[];
  imgsrc: string;
  onClick: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void;
  mode:
    | 'horizontal'
    | 'vertical'
    | 'vertical-left'
    | 'vertical-right'
    | 'inline';
  theme?: 'light' | 'dark';
  className: string;
}

export interface IFitHeaderItem {
  disabled?: boolean;
  key: string;
  title: string;
  icon?: ReactNode;
  danger?: boolean;
}
